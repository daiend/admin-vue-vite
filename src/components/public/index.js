import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const modules = import.meta.glob('./*.vue', { eager: true })

// 注册element-plus图标 | SVG图标
const iconInstall = {
	install: app => {
		for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
			app.component(key, component)
		}
		for (const path in modules) {
			app.component(modules[path].default.__name, modules[path].default)
		}
	}
}

export default iconInstall
