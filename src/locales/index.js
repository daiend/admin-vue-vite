import { createI18n } from 'vue-i18n'
import zh from './lang/zh.json'
import en from './lang/en.json'
const i18n = createI18n({
	legacy: false, // you must set `false`, to use Composition API
	locale: 'zh', // set locale
	messages: {
		zh,
		en
	}
})

export default i18n
