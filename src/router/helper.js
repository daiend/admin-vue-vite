// 导入组件

export const importComponent = path => () => import(/* @vite-ignore */ `/src/views/${path}.vue`)
// 构建路由
export const buildRouters = routes => {
	routes.forEach(route => {
		if (route.children && route.children.length) {
			buildRouters(route.children)
		}
		route.component = importComponent(route.component)
	})
}
