import { createRouter, createWebHistory } from 'vue-router'
import { useLocalStorage } from '@/hooks/public/useStorage.js'
import { buildRouters } from '@/router/helper.js'
import { STORAGE_PREFIX } from '@/config/app'
import axiosCanceler from '@/api/helper/AxiosCanceler.js'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
NProgress.configure({ showSpinner: false })
const storage = useLocalStorage()

const constantRouters = [
	{
		path: '/login',
		name: 'login',
		component: () => import('@/views/Login.vue')
	},
	{
		path: '/profile',
		name: 'profile',
		component: () => import('@/views/Profile.vue')
	},
	{
		path: '/:pathMatch(.*)*',
		name: 'NotFound',
		component: () => import('@/views/NotFound.vue')
	}
]

export const router = createRouter({
	history: createWebHistory(),
	routes: constantRouters
})

// 路由白名单
export const WHITE_LIST_NAME = ['login']
// 路由拦截
router.beforeEach(async to => {
	NProgress.start()
	axiosCanceler.removeAllPendingAxios()
	const token = storage.get(`${STORAGE_PREFIX}_token`)
	if (!token) {
		if (to.name && WHITE_LIST_NAME.includes(to.name)) {
			return true
		} else {
			return { path: '/login' }
		}
	} else {
		return true
	}
})
router.afterEach(() => {
	NProgress.done()
})

const routerInstall = {
	install(app) {
		const token = storage.get(`${STORAGE_PREFIX}_token`)
		if (token) {
			const menus = [...storage.get(`${STORAGE_PREFIX}_user`).menus]
			buildRouters(menus)
			menus.forEach(element => {
				router.addRoute(element)
			})
		}
		app.use(router)
	}
}

export default routerInstall
