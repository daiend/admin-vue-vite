import { createPinia } from 'pinia'
import { piniaPersistence } from './plugin.js'
const pinia = createPinia()

pinia.use(piniaPersistence)
export default pinia
