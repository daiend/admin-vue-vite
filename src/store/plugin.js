import { STORAGE_PREFIX } from '@/config/app.js'
import { useLocalStorage } from '@/hooks/public/useStorage.js'
const storage = useLocalStorage()
//pinia持久化
export function piniaPersistence(context) {
	context.store.$subscribe(() => {
		// 响应 store 变化
		storage.set(`${STORAGE_PREFIX}_${context.store.$id}`, context.store.$state)
	})
}
