import { defineStore } from 'pinia'
import { useLocalStorage } from '@/hooks/public/useStorage.js'
import { STORAGE_PREFIX } from '@/config/app.js'
const userSliceID = 'user'
const storage = useLocalStorage()
const storageUser = storage.get(`${STORAGE_PREFIX}_${userSliceID}`)
export const useUserStore = defineStore(userSliceID, {
	state: () =>
		storageUser || {
			name: '',
			avatar: '',
			introduction: '',
			roles: [],
			menus: [],
			permissions: []
		},
	actions: {
		logout() {
			this.$reset()
			storage.remove(`${STORAGE_PREFIX}_${userSliceID}`)
			storage.remove(`${STORAGE_PREFIX}_token`)
		}
	},
	getters: {
		getName: state => state.name,
		getAvatar: state => state.avatar,
		getIntroduction: state => state.introduction,
		getRoles: state => state.roles,
		getPermissions: state => state.permissions
	}
})
