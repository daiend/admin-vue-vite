import { defineStore } from 'pinia'
import { useLocalStorage } from '@/hooks/public/useStorage.js'
import themeConfig from '@/config/theme.js'
import { STORAGE_PREFIX } from '@/config/app'
const storage = useLocalStorage()
const themeSliceID = 'theme'
const storageTheme = storage.get(`${STORAGE_PREFIX}_${themeSliceID}`)
export const useThemeStore = defineStore(`${themeSliceID}`, {
	state: () => (storageTheme ? storageTheme : themeConfig),
	actions: {
		setDarkMode(darkMode) {
			this.darkMode = darkMode
		},
		setLayout(layout) {
			this.layout = layout
		},
		setPrimary(primary) {
			this.primary = primary
		},
		setGrayscale(grayscale) {
			this.grayscale = grayscale
		},
		setSize(size) {
			this.size = size
		}
	}
})
