import { defineStore } from 'pinia'
import { useLocalStorage } from '@/hooks/public/useStorage.js'
import appConfig, { STORAGE_PREFIX } from '@/config/app.js'
import { fullScreenFun, unFullScreenFun } from '@/utils/tools'
const appSliceID = 'app'
const storage = useLocalStorage()
const storageApp = storage.get(`${STORAGE_PREFIX}_${appSliceID}`)
export const useAppStore = defineStore(appSliceID, {
	state: () => (storageApp ? storageApp : appConfig),
	actions: {
		setLocale(locale) {
			this.locale = locale
		},
		setFullScreen(fullScreen) {
			fullScreen ? fullScreenFun() : unFullScreenFun()
			this.fullScreen = fullScreen
		}
	}
})
