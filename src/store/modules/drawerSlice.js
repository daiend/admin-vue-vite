import { defineStore } from 'pinia'
import { STORAGE_PREFIX } from '@/config/app.js'
import { ElMessageBox } from 'element-plus'
const drawerSliceID = 'drawer'

export const useDrawerStore = defineStore(`${STORAGE_PREFIX}_${drawerSliceID}`, {
	state: () => ({
		openedDrawers: {} //已打开的弹层对象
	}),
	actions: {
		/**打开弹窗drawer方法
		 * @param {*} params
		 * {
		 * title: 弹窗标题,
		 * type:弹窗类型 view：查看,edit：编辑, add：新增,
		 * name:弹窗名,用作关闭key
		 * }
		 */
		openDrawer(params) {
			const defaultParams = {
				title: '',
				type: 'view',
				name: ''
			}
			this.openedDrawers[`${params.name}`] = { ...defaultParams, ...params }
		},
		/**
		 * @param {*} name
		 * name:弹窗名,用作关闭key
		 * bool: true ,直接关闭
		 */
		//关闭弹窗
		closeDrawer(name, bool) {
			if (bool) {
				delete this.openedDrawers[name]
				return
			}
			if (this.openedDrawers[name].type != 'view') {
				ElMessageBox.confirm('确定要关闭吗?', '温馨提示', {
					confirmButtonText: '确定',
					cancelButtonText: '取消',
					type: 'warning'
				})
					.then(() => {
						delete this.openedDrawers[name]
					})
					.catch(() => {
						// catch error
					})
			} else {
				delete this.openedDrawers[name]
			}
		}
	},

	getters: {
		getOpenDrawer: state => {
			return name => state.openedDrawers[name]
		}
	}
})
