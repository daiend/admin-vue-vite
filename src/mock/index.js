import Mock from 'mockjs'
const modules = import.meta.glob('./modules/*.js', { eager: true })
let mock
for (const path in modules) {
	modules[path].default.forEach(element => {
		Mock.mock(`${import.meta.env.DAI_PROXY_PREFIX}${element.url}`, element.method, element.handle)
	})
}
if (import.meta.env.MODE === 'development') {
	mock = Mock
}
export default mock
