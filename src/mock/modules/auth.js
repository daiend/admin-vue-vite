const user = [
	{
		neme: 'admin',
		password: '123456',
		token: 'admin-token'
	},
	{
		neme: 'editor',
		password: '123456',
		token: 'editor-token'
	}
]
const mockList = [
	{
		url: '/login',
		method: 'post',
		handle: config => {
			let token
			if (config.body) {
				const account = JSON.parse(config.body)
				user.forEach(item => {
					if (item.neme === account.username && item.password === account.password) {
						token = item.token
					}
				})
			}
			if (!token) {
				return {
					status: 400,
					data: {},
					message: '用户名或密码错误!'
				}
			}
			return { code: 200, data: token, message: '成功' }
		}
	},
	{
		url: '/user/info',
		method: 'get',
		handle: () => {
			return {
				code: 200,
				data: {
					name: '平台管理员',
					avatar: '',
					introduction: '平台管理员',
					roles: ['admin'],
					permissions: [],
					menus: [
						{
							path: '/',
							name: 'home',
							meta: {
								icon: 'home',
								zhName: '首页',
								enName: 'home'
							},
							component: 'Home'
						},
						{
							path: '/system',
							name: 'system',
							meta: {
								icon: 'system',
								zhName: '系统管理',
								enName: 'system'
							},
							component: 'View',
							children: [
								{
									name: 'organize',
									path: '',
									component: 'system/organize',
									meta: {
										zhName: '组织',
										enName: 'organize',
										icon: 'organize'
									}
								},
								{
									name: 'post',
									path: 'post',
									component: 'dashboard/post',
									meta: {
										zhName: '岗位',
										enName: 'post',
										icon: 'post'
									}
								},
								{
									name: 'menu',
									path: 'menu',
									component: 'system/menu',
									meta: {
										zhName: '菜单',
										enName: 'menu',
										icon: 'menu'
									}
								},
								{
									name: 'role',
									path: 'role',
									component: 'system/role',
									meta: {
										zhName: '角色',
										enName: 'role',
										icon: 'role'
									}
								},
								{
									name: 'user',
									path: 'user',
									component: 'system/user',
									meta: {
										zhName: '用户',
										enName: 'user',
										icon: 'user'
									}
								}
							]
						},
						{
							path: '/cockpit',
							name: 'cockpit',
							meta: {
								icon: 'cockpit',
								zhName: '驾驶舱',
								enName: 'cockpit'
							},
							component: 'Cockpit'
						}
					]
				},
				message: '成功'
			}
		}
	}
]
export default mockList
