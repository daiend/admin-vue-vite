import axios from 'axios'
import { useLocalStorage } from '@/hooks/public/useStorage.js'
import router from '@/router/index.js'
import { ElMessage } from 'element-plus'
import { STORAGE_PREFIX } from '@/config/app.js'
import { responseTypes, successCode, tansParams } from './utils.js'
import axiosCanceler from './AxiosCanceler.js'

const storage = useLocalStorage()
const service = axios.create({
	baseURL: import.meta.env.DAI_PROXY_PREFIX, // url = base url + request url
	timeout: 5000 // request timeout
})
service.interceptors.request.use(
	config => {
		if (config.method === 'get' && config.params) {
			// 重新编码
			let url = config.url + '?' + tansParams(config.params)
			url = url.slice(0, -1)
			config.params = {}
			config.url = url
		}
		const token = storage.get(`${STORAGE_PREFIX}_token`)
		if (token) {
			config.headers.token = token
		}
		axiosCanceler.addPendingAxios(config)
		return config
	},
	error => {
		return Promise.reject(error)
	}
)

service.interceptors.response.use(
	response => {
		if (response.config.responseType && responseTypes.includes(response.config.responseType)) {
			return response
		}
		const data = response.data
		if (successCode.includes(data.code)) {
			return data
		} else {
			ElMessage({
				message: data.message,
				type: 'error'
			})
			return Promise.reject(data.message)
		}
	},
	error => {
		if (error.response) {
			switch (error.response.status) {
				case 400:
					error.message = '请求错误'
					break
				case 401:
					error.message = '未授权，请登录'
					storage.remove(`${STORAGE_PREFIX}_token`)
					storage.remove(`${STORAGE_PREFIX}_user`)
					storage.remove(`${STORAGE_PREFIX}_app`)
					router.push('/login')
					break
				case 403:
					error.message = '拒绝访问'
					break
				case 404:
					error.message = `请求地址出错: ${error.response.config.url}`
					break
				case 408:
					error.message = '请求超时'
					break
				case 500:
					error.message = '服务器内部错误'
					break
				case 501:
					error.message = '服务未实现'
					break
				case 502:
					error.message = '网关错误'
					break
				case 503:
					error.message = '服务不可用'
					break
				case 504:
					error.message = '网关超时'
					break
				case 505:
					error.message = 'HTTP版本不受支持'
					break
				default:
					error.message = `连接错误${error.response.status}`
			}
		} else {
			error.message = '连接到服务器失败'
		}
		ElMessage({
			message: error.message,
			type: 'error',
			duration: 5 * 1000
		})
		return Promise.reject(error)
	}
)

export default service
