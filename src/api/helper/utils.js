export const getPendingUrl = config => {
	return [config.method, config.url].join('&')
}

/**
 * 参数处理[对象转参数 || 参数转义]
 * @param {*} params  参数
 */
export function tansParams(params) {
	let result = ''
	for (const propName of Object.keys(params)) {
		const value = params[propName]
		const part = encodeURIComponent(propName) + '='
		if (value !== null && value !== '' && typeof value !== 'undefined') {
			if (typeof value === 'object') {
				result += part + encodeURIComponent(JSON.stringify(value)) + '&'
			} else {
				result += part + encodeURIComponent(value) + '&'
			}
		}
	}
	return result
}

export const responseTypes = ['blob', 'arraybuffer']

export const successCode = [200, 201, 202, 204]
