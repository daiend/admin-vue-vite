import { getPendingUrl } from './utils.js'

// 用于存储每个请求的标识和取消函数
const pendingRequestMap = new Map()

class AxiosCanceler {
	/**
	 * 添加请求
	 * @param config 请求配置
	 */
	addPendingAxios(config) {
		this.removePendingAxios(config)
		const url = getPendingUrl(config)
		const controller = new AbortController()
		config.signal = controller.signal
		if (!pendingRequestMap.has(url)) {
			// 如果当前请求不在等待中，将其添加到等待中
			pendingRequestMap.set(url, controller)
		}
	}
	/**
	 * 清除所有等待中的请求
	 */
	removeAllPendingAxios() {
		pendingRequestMap.forEach(abortController => {
			if (abortController) {
				abortController.abort()
			}
		})
		this.resetPendingAxios()
	}
	/**
	 * 移除请求
	 * @param config 请求配置
	 */
	removePendingAxios(config) {
		const url = getPendingUrl(config)
		if (pendingRequestMap.has(url)) {
			// 如果当前请求在等待中，取消它并将其从等待中移除
			const abortController = pendingRequestMap.get(url)
			if (abortController) {
				abortController.abort(url)
			}
			pendingRequestMap.delete(url)
		}
	}
	/**
	 * 重置
	 */
	resetPendingAxios() {
		pendingRequestMap.clear()
	}
}

const axiosCanceler = new AxiosCanceler()
export default axiosCanceler
