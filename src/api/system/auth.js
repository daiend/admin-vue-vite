import request from '@/api/helper/request.js'

/**
 * 登录
 * @param data
 */
export const loginApi = data => {
	return request({
		url: '/login',
		method: 'post',
		data
	})
}
// 获取用户信息
export const getUserInfoApi = () => {
	return request({
		url: '/user/info',
		method: 'get'
	})
}
