/**
 * @description 判断是否未定义
 * @param value
 * @returns boolean
 */
export function isUnDef(value) {
	return typeof value === 'undefined'
}

/**
 * @description 判断是否为null
 * @param value
 * @returns boolean
 */
export function isNull(value) {
	return value === null
}

export function isNullOrUnDef(value) {
	return isUnDef(value) || isNull(value)
}
