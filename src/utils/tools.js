/**
 *  引入图片
 * @param {*} path  相对于'/src/assets/img/' 图片文件路径
 * @returns
 */
export const requireImg = path => {
	return new URL(`../assets/img/${path}`, import.meta.url).href
}

/**
 * 全屏
 * 任何自动加载全屏的代码都无法主动启动浏览器的全屏，
 * 无论是异步的还是模拟点击事件也不行！
 * 只有用户的主动行为才可以触发（任何鼠标事件任何键盘事件等等）
 */
export const fullScreenFun = () => {
	var docElm = document.documentElement
	//W3C
	if (docElm.requestFullscreen) {
		docElm.requestFullscreen()
	}

	//FireFox
	else if (docElm.mozRequestFullScreen) {
		docElm.mozRequestFullScreen()
	}

	//Chrome等
	else if (docElm.webkitRequestFullScreen) {
		docElm.webkitRequestFullScreen()
	}

	//IE11
	else if (docElm.msRequestFullscreen) {
		docElm.msRequestFullscreen()
	}
}
/**
 * 退出全屏
 */
export const unFullScreenFun = () => {
	var isFullscreen =
		document.fullscreenElement ||
		document.msFullscreenElement ||
		document.mozFullScreenElement ||
		document.webkitFullscreenElement ||
		false
	if (isFullscreen) {
		//W3C
		if (document.exitFullscreen) {
			document.exitFullscreen()
		}
		//FireFox
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen()
		}
		//Chrome等
		else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen()
		}
		//IE11
		else if (document.msExitFullscreen) {
			document.msExitFullscreen()
		}
	}
}
