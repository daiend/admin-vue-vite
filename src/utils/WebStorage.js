import AesEncryption from './AesEncryption.js'
import { isNullOrUnDef } from './is.js'
import { CIPHER } from '@/config/app.js'
export class WebStorage {
	constructor(params) {
		this.storage = params.storage || localStorage
		this.encryption = new AesEncryption(CIPHER)
		this.isEncryption = params.encrypt || false
		this.defaultExpire = params.expire || Infinity
	}

	/**
	 * 存储数据
	 * @param {string} key 键
	 * @param {unknown} value 值
	 * @param {boolean} [encrypt] 是否加密
	 * @param {number} [expire] 过期时间，单位毫秒
	 */
	set(key, value, encrypt = this.isEncryption, expire = this.defaultExpire) {
		const stringData = JSON.stringify({
			value,
			expire: expire ? Date.now() + expire : null
		})

		const encryptData = encrypt ? this.encryption.aseEncrypt(stringData) : stringData
		this.storage.setItem(key, encryptData)
	}
	/**
	 * 获取存储值
	 * @param {string} key 键
	 * @param {boolean} [decrypt] 是否解密
	 * @returns 存储值
	 */
	get(key, decrypt = this.isEncryption) {
		const dataString = this.storage.getItem(key)
		if (!dataString) return null
		try {
			const decryptData = decrypt ? this.encryption.aseDecrypt(dataString) : dataString
			const data = JSON.parse(decryptData)
			const { value, expire } = data
			if (isNullOrUnDef(expire) || expire >= new Date().getTime()) {
				return value
			} else {
				this.remove(key)
				return null
			}
		} catch (e) {
			return null
		}
	}

	/**
	 * 删除
	 * @param {string} key
	 */
	remove(key) {
		this.storage.removeItem(key)
	}
	/**
	 * 清空
	 */
	clear() {
		this.storage.clear()
	}
}
