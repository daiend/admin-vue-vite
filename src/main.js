import { createApp } from 'vue'
import App from './App.vue'
import i18n from './locales'
import 'element-plus/theme-chalk/display.css'
import '@/assets/scss/daiendTheme/index.scss'
import '@/assets/scss/elementTheme/index.scss'
import ElementPlus from 'element-plus'
import store from './store'
import router from './router'
import iconInstall from './components/public'

import 'virtual:windi.css'
import 'virtual:svg-icons-register'
//mock
import '@/mock'
const app = createApp(App)
app.use(store)
app.use(router)
app.use(i18n)
app.use(ElementPlus)
app.use(iconInstall)
app.mount('#app')
