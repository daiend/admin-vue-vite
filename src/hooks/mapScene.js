import Map from 'ol/Map'
import View from 'ol/View'
import TileLayer from 'ol/layer/Tile'
import XYZ from 'ol/source/XYZ'
import 'ol/ol.css'
import { img_c } from '@/config/map.js'
import { ref, nextTick } from 'vue'
export const useMap = () => {
	const map = ref(null)
	const mapRef = ref(null)

	const initMap = () => {
		// 创建地图
		const center = [114.1692, 30.494] //EPSG:4326
		const view = new View({
			center: center, //EPSG:4326
			projection: 'EPSG:4326',
			zoom: 10
		})

		let img_w_url = img_c

		let img_w = new TileLayer({
			source: new XYZ({
				url: img_w_url
			})
		})
		nextTick(() => {
			map.value = new Map({
				target: mapRef.value,
				view: view,
				layers: [img_w]
			})
		})
	}

	return { mapRef, initMap, map }
}
