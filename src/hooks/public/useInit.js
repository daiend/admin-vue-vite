import { useTheme } from './useTheme.js'
import { useThemeStore } from '@/store/modules/themeSlice.js'
import { useI18n } from 'vue-i18n'
import { useAppStore } from '@/store/modules/appSlice.js'
export const useInit = () => {
	/**
	 * 初始化主题
	 */
	const initTheme = () => {
		const themeStore = useThemeStore()
		const { switchDark, changePrimary, switchGrayscale } = useTheme()
		switchDark(themeStore.darkMode)
		changePrimary(themeStore.primary)
		switchGrayscale(themeStore.grayscale)
	}
	/**
	 * 初始化应用
	 */
	const initApp = () => {
		const appStore = useAppStore()
		const { locale } = useI18n()
		locale.value = appStore.locale
	}
	return {
		initTheme,
		initApp
	}
}
