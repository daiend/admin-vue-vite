//浏览器持久化存储

import { WebStorage } from '@/utils/WebStorage.js'
import { DEFAULT_EXPIRE } from '@/config/app.js'
export const useSessionStorage = () => {
	return new WebStorage({ storage: sessionStorage, encrypt: true, expire: DEFAULT_EXPIRE })
}
// localStorage
export const useLocalStorage = () => {
	return new WebStorage({ storage: localStorage, encrypt: true, expire: DEFAULT_EXPIRE })
}

export default useLocalStorage()
