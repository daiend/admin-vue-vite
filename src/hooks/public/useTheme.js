import { CLASS_PREFIX } from '@/config/app.js'

export const useTheme = () => {
	/**
	 * @description: 切换主题
	 * @param {boolean} darkMode
	 */
	const switchDark = darkMode => {
		const html = document.documentElement
		//  ViewTransition
		darkMode ? html.setAttribute('class', 'dark') : html.setAttribute('class', '')
	}

	/**
	 * @description: 改变主题色
	 * @param color 主题色
	 */
	const changePrimary = color => {
		document.documentElement.style.setProperty(`--${CLASS_PREFIX}-color-primary`, color)
	}

	/**
	 * @description: 滤镜：灰度
	 * @param isGrey 是否灰度
	 */
	const switchGrayscale = grayscale => {
		const body = document.body
		grayscale ? body.setAttribute('style', 'filter: grayscale(1)') : body.setAttribute('style', 'filter: grayscale(0)')
	}

	return {
		switchDark,
		changePrimary,
		switchGrayscale
	}
}
