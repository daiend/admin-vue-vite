/**
 * 主题相关默认配置
 */

// 默认不是暗黑模式模式
export const DEFAULT_DARK_MODE = false
// 默认布局
export const DEFAULT_LAYOUT = 'vertical'
// 默认主题颜色
export const DEFAULT_PRIMARY = '#1677ff'
// 默认不是灰度滤镜
export const DEFAULT_GRAYSCALE = false
// 默认尺寸
export const DEFAULT_SIZE = 'default'

export default {
	darkMode: DEFAULT_DARK_MODE,
	layout: DEFAULT_LAYOUT,
	primary: DEFAULT_PRIMARY,
	grayscale: DEFAULT_GRAYSCALE,
	size: DEFAULT_SIZE
}
