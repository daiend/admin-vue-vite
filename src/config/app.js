// 存储前缀
export const STORAGE_PREFIX = 'dai'
// 样式前缀
export const CLASS_PREFIX = 'dai'

//加密密钥和初始向量
// key：秘钥  16位
// iv：初始向量(Initialization Vector)，目的是防止同样的明文块，始终加密成同样的密文块
export const CIPHER = {
	key: '_11111222211111_',
	iv: '_22222111122222_'
}

// 默认持久化缓存过期时长(毫秒)：一周 1000*60*60*24*7 = 604800000
export const DEFAULT_EXPIRE = 604800000

export const appTitle = 'admin-vue-vite'
// 默认配置
const appConfig = {
	locale: 'zh',
	fullScreen: false
}

export default appConfig
