/* eslint-disable no-undef */
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import { resolve } from 'path'
import { createHtmlPlugin } from 'vite-plugin-html'
import { appTitle } from './src/config/app'
// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
	// 根据当前工作目录中的 `mode` 加载 .env 文件
	// 设置第三个参数为 '' 来加载所有环境变量，而不管是否有 `DAI_` 前缀。
	const env = loadEnv(mode, process.cwd(), '')
	return {
		// vite 配置
		root: process.cwd(), //项目根目录（index.html 文件所在的位置）
		base: env.DAI_DEPLOYMENT_PATH, //部署路径
		define: {
			//定义全局常量
			__APP_ENV__: JSON.stringify(env.APP_ENV)
		},
		plugins: [
			WindiCSS(),
			vue({
				template: {
					compilerOptions: {
						// treat all tags with a dash as custom elements
						isCustomElement: tag => tag.includes('swiper-')
					}
				}
			}),
			createSvgIconsPlugin({
				// 指定需要缓存的图标文件夹
				// eslint-disable-next-line no-undef
				iconDirs: [resolve(process.cwd(), 'src/assets/svg')],
				// 指定symbolId格式
				symbolId: 'svg-[dir]-[name]'
			}),
			createHtmlPlugin({
				inject: {
					data: { title: appTitle }
				}
			})
		], //插件
		publicDir: 'public', //静态资源的文件夹
		resolve: {
			alias: {
				//文件系统路径的别名
				'@': resolve(__dirname, './src')
			}
		},
		css: {
			preprocessorOptions: {
				scss: {
					// 允许全局变量
					// additionalData: `@import "@/assets/scss/variables.scss";`,
				}
			}
		},
		envPrefix: 'DAI_', //以 envPrefix 开头的环境变量会通过 import.meta.env 暴露在你的客户端源码中
		server: {
			host: '0.0.0.0', // 开发服务器监听的IP地址，设置为 0.0.0.0 或者 true 将监听所有地址，包括局域网和公网地址
			port: 7777, // 开发服务器端口
			open: true, // 开发服务器启动时，自动在浏览器中打开应用程序
			proxy: {
				//开发服务器配置自定义代理规则
				// 代理配置
				// 使用 proxy 实例
				[env.DAI_PROXY_PREFIX]: {
					target: env.DAI_API_URL,
					changeOrigin: true,
					rewrite: path => path.replace(/^\/api/, '')
					// configure: (proxy, options) => {
					// 	// proxy 是 'http-proxy' 的实例
					// },
				}
			},
			build: {
				// 生产环境构建文件的目录
				outDir: 'dist'
			}
		}
	}
})
