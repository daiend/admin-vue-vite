//https://prettier.nodejs.cn/docs/en/options.html
export default {
	experimentalTernaries: false, //三元运算式?是否换行
	printWidth: 120, //打印宽度(换行宽度)
	useTabs: true, //是否使用tab缩进
	tabWidth: 2, //tab缩进的空格数
	semi: false, //语句末尾是否有分号
	singleQuote: true, //是否使用单引号
	trailingComma: 'none', //是否使用尾随逗号,数据最后一行是否有逗号
	quoteProps: 'as-needed', //对象属性是否使用引号,仅在需要的对象属性周围添加引号
	bracketSpacing: true, //对象大括号内的空格是否有空格
	bracketSameLine: true, //将多行 HTML（HTML、JSX、Vue、Angular）元素的 > 放在最后一行的末尾
	arrowParens: 'avoid', //箭头函数括号尽可能省略
	htmlWhitespaceSensitivity: 'css', //html中空格敏感度:遵守 CSS display 属性的默认值
	vueIndentScriptAndStyle: true, //在 Vue 文件中缩进脚本和样式标签
	endOfLine: 'lf' //换行符
}
